package models

import anorm.{~, SQL, Row}
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current

case class Pick(id: Option[Long], user1: String, user2: String) {

    /**
     * save a pick in the database
     */
    def save: Pick = {
        if (!id.isEmpty) {
            //update the pick
            play.Logger.debug("Update Pick Not implemented")
            this
        } else {
            //insert
            DB.withConnection {
                implicit connection =>
                    val id: Option[Long] = SQL("insert into " + Pick.tableName + " (user1, user2) values " +
                        "({user1}, {user2})")
                        .on('user1 -> this.user1, 'user2 -> this.user2).executeInsert()
                    play.Logger.debug("inserted pick Id: " + id.get)
                    this.copy(id)
            }
        }
    }

    /**
     * remove a pick from the db by it's id
     */
    def delete = {
        if (!id.isEmpty) {
            DB.withConnection {
                implicit connection =>
                    val affected = SQL("delete from " + Pick.tableName + " where id = {cid}")
                        .on('cid -> this.id).executeUpdate()
            }
        } else {
            play.Logger.error("No pick id")
        }
    }
}

object Pick {
    val tableName = "picks"

    /**
     * find an image by it's id
     * @param id The image id
     */
    def findById(id: Long): Option[Pick] = {
        DB.withConnection {
            implicit connection =>
                anorm.SQL("select * from " + tableName + " where id = {id}").as(simple.singleOpt)
        }
    }

    val simple = {
        get[Option[Long]](tableName + ".id") ~
        get[String](tableName + ".user1") ~
        get[String](tableName + ".user2") map {
            case id ~ user1 ~ user2 => {
                models.Pick(id, user1, user2)
            }
        }
    }

    val count = {
        get[Option[Long]]("count(*)") map {
            case Some(count:Long) => count
            case _ => 0L
        }
    }

    def saveList(dateId: String, picks: List[String]) = {
        play.Logger.debug(picks.toString)
        DB.withConnection {
            implicit connection =>
                picks.map { pickId =>
                    val pid = pickId.toLowerCase
                    val thing = SQL("select * from " + tableName + " where user1 = {did} and user2 = {pid}")
                        .on('did -> dateId, 'pid -> pid).as(simple.singleOpt)
                    if (thing.isEmpty) {
                        val id: Option[Long] = SQL("insert into " + tableName + " (user1, user2) values ({u1}, {u2})")
                            .on('u1 -> dateId, 'u2 -> pid).executeInsert()
                        play.Logger.debug(id.get.toString)
                    }
                }
        }
    }

    /**
     * return all picks
     */
     def list: List[Pick] = {
        DB.withConnection {
            implicit connection =>
                val rows = anorm.SQL(s"select count(*) from $tableName").as(count.single)
                if (rows > 0) {
                    anorm.SQL("select * from " + tableName).as(simple.*)
                } else List[Pick]()
        }
     }

/*    def list: List[(Long, Long)] = {
        DB.withConnection {
            implicit connection =>
                anorm.SQL("select * from " + tableName).as(
                    long("user1") ~ long("user2") map(flatten) *)
        }
    }*/

    def listGenderSpecificPicks(gender: String): List[(String, String)] = {
        DB.withConnection {
            implicit connection =>
            val query = SQL("select user1, user2 from " + tableName + " where user1 like '%"+gender+"%'")
            query().map(row=>
                row[String]("user1") -> row[String]("user2")
            ).toList
        }
    }

}