package models

import anorm.{SQL, ~}
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current

case class Admin(id: Option[Long], username: String, password: String, clearance: String) {

    /**
     * save a admin in the database
     */
    def save: Admin = {
        if (!id.isEmpty) {
            //update the admin
            play.Logger.debug("Update Admin info not implemented.")
            this
        } else {
            //insert
            DB.withConnection {
                implicit connection =>
                    val id: Option[Long] = SQL("insert into " + Admin.tableName + 
                        " (username, password, clearance) values " +
                        "({username}, {password}, {clearance})")
                        .on('username -> this.username.toLowerCase, 'password -> this.password, 'clearance -> this.clearance.toLowerCase).executeInsert()
                    play.Logger.info("inserted admin Id: " + id.get)
                    this.copy(id)
            }
        }
    }

    /**
     *  Add the admin dateId
     */
    private def addId(id: Option[Long]): Admin = {
        if (!id.isEmpty) {
            DB.withConnection {
                implicit connection =>
                val did: String = id.get+this.clearance(0).toString.toUpperCase
                play.Logger.debug("DateId: " + did)
                val rows = SQL("update " + Admin.tableName + " set dateId = {did} where id = {uid}")
                    .on('did -> did, 'uid -> id).executeUpdate()
                play.Logger.debug("Rows updooted: " + rows.toString)
                this
            }
        } else {
            play.Logger.error("Cannot update unidentified admin")
            this
        }
    }

    /**
     * remove a admin from the db by it's id
     */
    def delete = {
        if (!id.isEmpty) {
            DB.withConnection {
                implicit connection =>
                    val affected = SQL("delete from " + Admin.tableName + " where id = {uid}")
                        .on('uid -> this.id).executeUpdate()
            }
        } else {
            play.Logger.error("Cannot Delete Admin Without Id")
        }
    }
}

object Admin {
    val tableName = "admin"

    /**
     * find a admin by their id
     * @param id The admin id
     * @return admin
     */
    def findById(id: Long): Option[Admin] = {
        DB.withConnection {
            implicit connection =>
                SQL("select * from " + tableName + " where id = {id}").on('id -> id).as(simple.singleOpt)
        }
    }

    /**
     * Find an admin by username
     * @param username The username to look up
     * @return true if the id is found, flase otherwise
     */
    def exists(username: String): Boolean = {
        DB.withConnection {
            implicit connection =>
                val id = SQL(s"select id from $tableName where username = {username}").on('username -> username)
                .as(scalar[Long].singleOpt)
                if (id.isEmpty) false else true
        }
    }

    def findByUsername(username: String): Option[Admin] = {
        DB.withConnection {
            implicit connection =>
                SQL(s"select * from $tableName where username = {username}").on('username -> username)
                .as(simple.singleOpt)
        }
    }

    /**
     * Checks if adding admins is allowed
     * TODO: Add table with preferences
     */
    def canCurrentlyAddNewAdmins: Boolean = {
        DB.withConnection {
            implicit connection =>
                true
        }
    }

    /**
     * Parses a result set row into a admin object
     */
    val simple = {
        get[Option[Long]](tableName + ".id") ~
        get[String](tableName + ".username") ~
        get[String](tableName + ".password") ~
        get[String](tableName + ".clearance") map {
            case id ~ username ~ password ~ clearance => {
                models.Admin(id, username, password, clearance)
            }
        }
    }

    val justId = {
        get[Option[Long]](tableName + ".id") map {
            case Some(_:Long) => true
            case None => false
        }
    }

    /**
     * Get all Admins in the database
     */
    def list: List[Admin] = {
        DB.withConnection {
            implicit connection =>
                SQL("select * from " + tableName).as(simple*)
        }
    }

}