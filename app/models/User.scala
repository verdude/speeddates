package models

import anorm.{SQL, ~}
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current

case class User(id: Option[Long], firstName: String, lastName: String, gender: String,
    email: String, phone: String, var dateId: String, matched: Boolean = false) {

    /**
     * save a user in the database
     */
    def save: Option[User] = {
        if (!id.isEmpty) {
            //update the user
            play.Logger.debug("Update User info not implemented.")
            Some(this)
        } else {
            //insert
            DB.withConnection {
                implicit connection =>
                    // first check if email address is already in the database.
                    val emailId = SQL("select id from " + User.tableName + " where email = {email}").on('email -> this.email)
                        .as(scalar[Long].singleOpt)
                    if (emailId.isEmpty) {
                        val id: Option[Long] = SQL("insert into " + User.tableName + 
                            " (firstName, lastName, gender, email, phone, dateId, matched) values " +
                            "({firstName}, {lastName}, {gender}, {email}, {phone}, {dateId}, 0)")
                            .on('firstName -> this.firstName.toLowerCase, 'lastName -> this.lastName, 'gender -> this.gender.toLowerCase, 
                                'email -> this.email, 'phone -> this.phone, 'dateId -> this.dateId).executeInsert()
                        play.Logger.info("inserted user Id: " + id.get)
                        if (this.dateId != "") {
                            Some(this.copy(id))
                        } else {
                            Some(this.copy(id).addId(id))
                        }
                    } else {
                        None
                    }
            }
        }
    }

    /**
     * Mark the user as matched
     */
    def markAsMatched: Int = {
        DB.withConnection {
            implicit connection =>
            val matchDuplicates = SQL("update " + User.tableName + 
                " set matched = 1 where firstName = {fname} and lastName = {lname} and email = {email} and phone = {phone}")
                .on('fname -> firstName, 'lname -> lastName, 'email -> email, 'phone -> phone).executeUpdate()
            play.Logger.debug(s"Set $firstName as matched everywhere.")
            matchDuplicates
        }
    }

    /**
     *  Add the user dateId
     */
    private def addId(id: Option[Long]): User = {
        if (!id.isEmpty) {
            DB.withConnection {
                implicit connection =>
                val did: String = id.get+this.gender(0).toString.toUpperCase
                play.Logger.debug("DateId: " + did)
                val rows = SQL("update " + User.tableName + " set dateId = {did} where id = {uid}")
                    .on('did -> did, 'uid -> id).executeUpdate()
                play.Logger.debug("Rows updooted: " + rows.toString)
                this.dateId = did
                this
            }
        } else {
            play.Logger.error("Cannot update unidentified user")
            this
        }
    }

    /**
     * remove a user from the db by it's id
     */
    def delete = {
        if (!id.isEmpty) {
            DB.withConnection {
                implicit connection =>
                    val affected = SQL("delete from " + User.tableName + " where id = {uid}")
                        .on('uid -> this.id).executeUpdate()
            }
        } else {
            play.Logger.error("Cannot Delete User Without Id")
        }
    }

}

object User {
    val tableName = "users"

    /**
     * find a user by their id
     * @param id The user id
     * @return user
     */
    def findById(id: Long): Option[User] = {
        DB.withConnection {
            implicit connection =>
                SQL("select * from " + tableName + " where id = {id}").on('id -> id).as(simple.singleOpt)
        }
    }

    /**
     * Parses a result set row into a user object
     */
    val simple = {
        get[Option[Long]](tableName + ".id") ~
        get[String](tableName + ".firstName") ~
        get[String](tableName + ".lastName") ~
        get[String](tableName + ".gender") ~
        get[String](tableName + ".email") ~
        get[String](tableName + ".phone") ~
        get[String](tableName + ".dateId") ~
        get[Boolean](tableName + ".matched") map {
            case id ~ firstName ~ lastName ~ gender ~ email ~ phone ~ dateId ~ matched => {
                models.User(id, firstName, lastName, gender, email, phone, dateId, matched)
            }
        }
    }

    /**
     * Get all Users in the database
     */
    def list: List[User] = {
        DB.withConnection {
            implicit connection =>
                SQL("select * from " + tableName).as(simple*)
        }
    }

    def list(gender: String): List[User] = {
        DB.withConnection {
            implicit connection =>
                SQL("select * from " + tableName + " where gender like '%"+gender+"%'").as(simple*)
        }
    }

    def listNotMatched: List[User] = {
        DB.withConnection {
            implicit connection =>
                SQL("select * from " + tableName + " where matched = 0").as(simple*)
        }
    }

    /**
     * Get User in the database
     * @param firstName The User's first Name
     * @param dateId The user's dateId
     */
    def login(firstName: String, dateId: String): Option[User] = {
        DB.withConnection {
            implicit connection =>
                SQL("select * from " + tableName + " where firstName = {fn} and dateId = {did}")
                    .on('fn -> firstName.toLowerCase, 'did -> dateId.toLowerCase).as(simple.singleOpt)
        }
    }

    /**
     * get user by the date id
     */
    def findByDateId(dateId: String): Option[User] = {
        DB.withConnection {
            implicit connection =>
                SQL("select * from " + tableName + " where dateId = {id}").on('id -> dateId).as(simple.singleOpt)
        }
    }

    /**
     * get users by the date id
     */
    def findAllByDateId(dateId: String): List[User] = {
        DB.withConnection {
            implicit connection =>
                SQL("select * from " + tableName + " where dateId = {id}").on('id -> dateId).as(simple*)
        }
    }

}