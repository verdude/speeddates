package service

import play.api.Play
import play.api.Play.current
import play.api.mvc._
import javax.mail.Message.RecipientType
import scala.concurrent.{ExecutionContext, Future}
import models._
import collection.mutable._

object mail {

  implicit def stringToSeq(single: String): Seq[String] = Seq(single)
  implicit def liftToOption[T](t: T): Option[T] = Some(t)

  sealed abstract class MailType
  case object Plain extends MailType
  case object Rich extends MailType
  case object MultiPart extends MailType

  case class Mail(
    from: (String, String), // (email -> name)
    to: Seq[String],
    subject: String,
    message: String
  )

  object send {
    def a(mail: Mail) {
      import org.apache.commons.mail._

      val format = Plain

      val commonsMail: Email = format match {
        case Plain => new SimpleEmail().setMsg(mail.message)
      }

      // TODO Set authentication from your configuration, sys properties or w/e
      commonsMail.setHostName(Send.host)
      commonsMail.setSmtpPort(Send.port)
      commonsMail.setAuthenticator(new DefaultAuthenticator(Send.address, Send.password))
      commonsMail.setSSLOnConnect(true)
      // Can't add these via fluent API because it produces exceptions
      mail.to foreach (commonsMail.addTo(_))

      commonsMail.
        setFrom(mail.from._1, mail.from._2).
        setSubject(mail.subject).
        send()
    }
  }
}

object Send {

  val host = Play.configuration.getString("smtp.host").getOrElse("smtp.googlemail.com")
  val port = Play.configuration.getInt("smtp.port").getOrElse(465)
  val name = Play.configuration.getString("smtp.name").getOrElse("Tri-Ward Speed Dating")
  val address = Play.configuration.getString("smtp.address").getOrElse("speeddatingactivity@gmail.com")
  val password = Play.configuration.getString("smtp.password").get

  def matchedTemplate(user: User): String = {
    s"""
      |${user.firstName + " " + user.lastName}, thank you for participating in Tri-Ward Speed Dating.
      |You matched with the following people:
      |
    """.stripMargin
  }

  def unmatchedTemplate(name: String): String = {
    s"""
      |$name, you did not match with anyone.
      |Thank you for participating in the Tri-Ward Speed Dating Activity.
      |--
    """.stripMargin
  }

  def thankYous = {
    val unmatchedUsers = User.listNotMatched
    val unmatched = unmatchedUsers.map{ person=>
      (person.firstName + " " + person.lastName -> person.email)
    }.distinct
    //play.Logger.debug(unmatched.toString)
    unmatched.foreach { person =>
      mail.send a new mail.Mail(
        from = (address, name),
        to = Seq(person._2),
        subject = "Tri-Ward Speed Dating Activity Results",
        message = unmatchedTemplate(person._1)
      )
      play.Logger.debug(s"Sent thank you to ${person._1}")
    }
  }

  /**
   * Mails all of the users that have matched a list of the people they have matched with along with their phone numbers
   */
  def out(matches: List[(String, String)]) = {
    var mmap: Map[User, MutableList[User]] = Map()
    // get the user and send the emails
    matches map { match_ =>
      // use these lists to check for duplicates.
      val person1 = User.findAllByDateId(match_._1)
      val person2 = User.findAllByDateId(match_._2)

      // Some output to check for errors
      if (person1.length == 0) {
        play.Logger.error("Noone with this id : " + match_._1)
      } else if (person1.length > 1) {
        play.Logger.error("Too Many people with this id: " + match_._1)
      }
      if (person2.length == 0) {
        play.Logger.error("Noone with this id : " + match_._2)
      } else if (person2.length > 1) {
        play.Logger.error("Too many people with this id: " + match_._2)
      }

      // Add each person to each other
      if (person1.length == 1 && person2.length == 1) {
        if (mmap.contains(person1(0))) {
          // add person 2 to the list
          mmap(person1(0)) += person2(0)
        } else {
          // Create new list for person one
          mmap += (person1(0) -> MutableList(person2(0)))
        }
        if (mmap.contains(person2(0))) {
          // add person 1 to person 2 list
          mmap(person2(0)) += person1(0)
        } else {
          // create new list for person 2
          mmap += (person2(0) -> MutableList(person1(0)))
        }
      }
    }

    /***************************************************************\
     Setting the users as matched so sending thank you's is simpler
    \***************************************************************/
    mmap foreach { tup =>
      if (tup._1.markAsMatched < 1) {
        play.Logger.error(tup._1.dateId + " Had an issue being set to matched")
      } else {
        play.Logger.info("Updated User to Matched: " + tup._1.dateId)
      }
      
      val mlist = tup._2.filterNot(_ == tup._1)
      val numbers = mlist.map( thing =>
        thing.firstName+" "+ thing.lastName+" : " concat thing.phone
      ).distinct.mkString("\n")

      play.Logger.debug(tup._1.firstName + " " + tup._1.lastName +  " matched with : " + numbers)
      mail.send a new mail.Mail(
        from = (address, name),
        to = Seq(tup._1.email),
        subject = "Tri-Ward Speed Dating Activity Results",
        message = matchedTemplate(tup._1) + "\n" + numbers
      )
    }
  }
}