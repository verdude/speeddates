package controllers

import play.api._
import play.api.mvc._
import play.api.mvc.Results._
import models.{User, Pick}
import play.api.libs.ws.WS
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.io.Source
import collection.mutable._

import authentication._

object Application extends Controller {

  val title = "Tri-Ward Speed Dating"
  
  def index = Action {
    implicit request =>
      Ok(views.html.index())
  }

  /**
   * Adds a User to the database
   */
  def addUser = Action(parse.multipartFormData) {
    implicit request =>
      val params = request.body.dataParts.mapValues(_(0))
      val newUser = User(None, params("firstName").toString.trim.toLowerCase, params("lastName").toString.trim.toLowerCase,
          params("gender").toString.trim.toLowerCase, params("email").toString.trim.toLowerCase, params("phone").toString.trim, "")
      newUser.save match {
          case Some(_:User) => Ok
          case None => BadRequest("Email already in use.")
          case _ => {
              play.Logger.error("Incorrect or non-existent user id")
              InternalServerError
          }
      }
  }

  /**
   * Save the user picks in the db
   */
  def savePicks = Action(parse.multipartFormData) {
    implicit request =>
      val params = request.body.dataParts.mapValues(_(0))
      play.Logger.debug(params.toString)
      val picks = params("picks").split(",").map(_.trim.toLowerCase).toList
      play.Logger.debug(picks.toString)
      val dateId = params("dateId")
      Pick.saveList(dateId, picks)
      Ok
  }

  /**
   * user selects gender and is redirected to the registration page
   */
  def selectGender(gender: String) = Action {
    implicit request =>
    Ok(views.html.registration(title, gender))
  }

  /*
   * redirect user to the login page
   */
  def loginPage = Action {
    implicit request =>
      Ok(views.html.login(title))
  }

  /**
   * Login endpoint for users with dateID's
   */
  def login = Action(parse.urlFormEncoded) {
    implicit request =>
      val params = request.body.mapValues(_(0))
      val user = User.login(params("firstName"), params("dateId"))
      if (!user.isEmpty) {
        user match {
          case _:Option[User] => Redirect(controllers.routes.Application.pick(user.get.dateId)).withSession("dateId"->user.get.dateId.toString)
          case _ => {
            play.Logger.debug("User Not Found")
            BadRequest
          }
        }
      } else { Ok(views.html.login(title, "Name and Id did not match up")) } // Should implement as a flash scope rather than passing a string back
  }

  /**
   * Endpoint for the pick page
   */
  def pick(dateId: String) = Authentication.authenticatedUser() {
    implicit request =>
      implicit user =>
        if (dateId == user.dateId)
          Ok(views.html.pick(title, dateId)).withSession("dateId"->user.dateId.toString)
        else Ok(views.html.login(title)).flashing("error" -> "You are not logged in.").withNewSession
  }
  
}

/**
 * An attempt to implement custom error handling.
 * Not working as of now.
 */
object Global extends GlobalSettings {
  override def onHandlerNotFound(request: RequestHeader): Result = {
    NotFound(views.html.codes.notFound())
  }
  override def onError(request: RequestHeader, ex: Throwable) = {
    InternalServerError(views.html.codes.errorPage())
  }  
  override def onBadRequest(request: RequestHeader, error: String) = {
    BadRequest("Bad Request: " + error)
  }
}