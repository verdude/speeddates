package controllers.authentication

import scala.concurrent._
import ExecutionContext.Implicits.global
import play.api.mvc._
import play.api.Play.current
import models._
import controllers.Errors

import org.mindrot.jbcrypt.BCrypt
import play.libs.Crypto

/**
 * This controller does logging out and has a bunch of helpers for dealing with authentication and permissions.
 */
object Authentication extends Controller {

  val isHTTPS = current.configuration.getBoolean("HTTPS").getOrElse(false)

  /**
   * Login endpoint for admin's
   */
  def adminlogin = Action(parse.urlFormEncoded) {
    implicit request =>
      var admin = getAdminFromSession
      admin match {
        case Some(_:Admin) => {
          Redirect(controllers.routes.AdminOps.users()).flashing("success" -> s"Welcome ${admin.get.username}").withSession("adminId" -> admin.get.id.get.toString)
        }
        case _ => {
          val params = request.body.mapValues(_(0))
          admin = Admin.findByUsername(params("username"))
          if (admin.isEmpty) {
            Ok(views.html.admin.adminLogin()).flashing("error" -> "Incorrect Username").withNewSession
          } else {
            if (BCrypt.checkpw(params("password"), admin.get.password)) {
              Redirect(controllers.routes.AdminOps.users()).flashing("success" -> s"Welcome ${admin.get.username}").withSession("adminId" -> admin.get.id.get.toString)
            } else Ok(views.html.admin.adminLogin()).flashing("error" -> "Incorrect Password").withNewSession
          }

        }
      }
  }

  /**
   * Get the admin from the session
   */
  private def getAdminFromSession()(implicit request: RequestHeader): Option[Admin] = {
    request.session.get("adminId").flatMap( adminId => Admin.findById(adminId.toLong) )
  }

  /**
   * Get the user from the session
   */
  private def getUserFromSession()(implicit request: RequestHeader): Option[User] = {
    request.session.get("dateId").flatMap( dateId => User.findByDateId(dateId) )
  }

  /**
   * A generic action to be used on authenticated pages.
   * @param f The action logic. A curried function which, given a request and the authenticated admin, returns a result.
   * @return The result. Either a redirect due to not being logged in, or the result returned by f.
   */
  def authenticatedAction[A](parser: BodyParser[A] = BodyParsers.parse.anyContent)(f: Request[A] => Admin => Result) = Action(parser) {
    implicit request =>
      getAdminFromSession().map{ admin =>
          if (admin.clearance == "master") f(request)(admin)
          else Redirect(controllers.routes.Application.index().toString(), Map("path" -> List(request.path)))
            .flashing("alert" -> "You are not allowed to view this content.")
        }.getOrElse {
        Redirect(controllers.routes.Application.index().toString(), Map("path" -> List(request.path)))
          .flashing("alert" -> "You are not logged in")
      }
  }

  /**
   * Used to authenticate a user on the pick.scala.html page
   */
  def authenticatedUser[A](parser: BodyParser[A] = BodyParsers.parse.anyContent)(f:Request[A] => User => Result) = Action(parser) {
    implicit request =>
      getUserFromSession().map(user => f(request)(user)).getOrElse {
        Redirect(controllers.routes.Application.index().toString(), Map("path" -> List(request.path)))
          .flashing("alert" -> "You are not logged in")
      }
  }

}
