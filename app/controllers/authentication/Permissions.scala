package controllers.authentication

import models._


case class Permissions(adminId: Long, permissionLevel: String) {

	/**
	 * if the admin can view. if passed without a param, the admin must be "master" lvl
	 */
	def canView(lvl: String = ""): Boolean = {
		Permissions.perms indexOf permissionLevel match {
			case 0 if lvl == permissionLevel => true
			case 1 => true
			case _ => false
		}
	}

}

object Permissions {
	
	val perms = List("basic", "master")

	def getPermissions(admin: Admin): Permissions = {
		admin match {
			case Admin(_,_,_,"") => play.Logger.warn("Warning, admin has no clearance level.")
		}
		Permissions(admin.id.getOrElse(-1), admin.clearance)
	}

}