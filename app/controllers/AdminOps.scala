package controllers

import play.api._
import play.api.mvc._
import play.api.mvc.Results._
import models._
import authentication._
import service._

import org.mindrot.jbcrypt.BCrypt
import play.libs.Crypto


/**
 * Handles all of the routing to the admin dashboard
 */
object AdminOps extends Controller {

  def newAdminForm = Action {
    implicit request =>
      Ok(views.html.admin.addAdmin()).withNewSession
  }

  def logout = Action {
    implicit request =>
      Ok(views.html.index()).withNewSession
  }

  def loginPage = Action {
    implicit request =>
      Ok(views.html.admin.adminLogin()).withNewSession
  }

  def users = Authentication.authenticatedAction() {
    implicit request =>
      implicit admin =>
        Ok(views.html.admin.users(User.list)).withSession("adminId"->admin.id.get.toString)
  }

  def picks = Authentication.authenticatedAction() {
    implicit request =>
      implicit admin =>
        Ok(views.html.admin.picks(Pick.list, Application.title)).withSession("adminId"->admin.id.get.toString)
  }

  def matches = Authentication.authenticatedAction() {
    implicit request =>
      implicit admin =>
        Ok(views.html.admin.matches(Application.title)).withSession("adminId"->admin.id.get.toString)
  }

  /**
   * Add A new Admin
   *
   *
   */
  def createAdmin = Action(parse.urlFormEncoded) {
    implicit request =>
      if (!Admin.canCurrentlyAddNewAdmins) {
        play.Logger.debug("Attempted to add admin during block.")
        BadRequest
      } else {
        val params = request.body.mapValues(_(0))
        val username = params("username")
        val password = params("password")
        if (password != params("password2") || Admin.exists(username)) {
          BadRequest
        } else {
          val newAdmin = Admin(None, username, BCrypt.hashpw(password, BCrypt.gensalt()), "basic").save
          Redirect(controllers.routes.AdminOps.users()).flashing("success" -> s"Welcome ${newAdmin.username}").withSession("adminId" -> newAdmin.id.get.toString)
        }
      }
  }

  /**
   * Checks if the picks are equal
   * returns true if the two picks are equal
   */
  private def picksEqual(first: (String, String), second: (String, String)): Boolean = {
    (first._1.toLowerCase -> first._2.toLowerCase) == (second._1.toLowerCase -> second._2.toLowerCase)
  }

  /**
   * Find all matches
   * loops through the 'reverse female' picks and the male picks and checks for matches with the pickEquals function
   * sends mail through the service.Mail file
   */
  def findMatches = Action {
    val females = Pick.listGenderSpecificPicks("f").map(_.swap)
    val males = Pick.listGenderSpecificPicks("m")
    val matches = {for {
      g <- females
      d <- males
      if picksEqual(g, d)
    } yield (g)}.distinct
    // Same Gender Matching...
    // matches += females.filter(_.1.toLowerCase == _.2.toLowerCase)
    // matches += males.filter(_.1.toLowerCase == _.2.toLowerCase)
    Send out matches
    Send.thankYous
    Ok
  }
}
