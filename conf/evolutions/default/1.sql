# data model v 1.0

# --- !Ups

CREATE TABLE users (
    id          bigint not null auto_increment,
    firstName   varchar(255) not null,
    lastName    varchar(255) not null,
    gender      varchar(255) not null,
    email       varchar(255) not null,
    phone       varchar(255) not null,
    dateId      varchar(255),
    primary key(id)
);

CREATE TABLE picks (
    id          bigint not null auto_increment,
    user1       varchar(255) not null,
    user2       varchar(255) not null,
    primary key(id)
);

# --- !Downs

drop table if exists users;
drop table if exists picks;