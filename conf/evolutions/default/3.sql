# --- !Ups

CREATE TABLE admin (
    id          bigint not null unique auto_increment,
    username    varchar(255) not null unique,
    password    varchar(255) not null,
    clearance   varchar(255) not null
);

# --- !Downs

DROP TABLE IF EXISTS admin;