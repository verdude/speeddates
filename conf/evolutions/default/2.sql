# --- Adding matched indicator

# --- !Ups

alter table users
	add column matched boolean not null default 0;

# --- !Downs

alter table users drop column users;