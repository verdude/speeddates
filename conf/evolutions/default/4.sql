# --- !Ups
ALTER TABLE users ADD UNIQUE (email);
# --- !Downs
# Drops the unique constraint
ALTER TABLE users DROP INDEX (email);
