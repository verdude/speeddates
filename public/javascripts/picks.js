$(function(){
    Ractive.DEBUG = false;
    var list, template1 = 
    '<div class="row"><div class="offset-by-two four columns">\
    <input type="text" id="new" on-change="add" placeholder="Date ID">\
    </div></div>\
    <ul>\
        {{#ids:i}}<div class="row"><div id="i" on-click="delete:i" class="offset-by-two four columns"><li>\
            {{ids[i]}} &times;\
        </li></div></div>{{/ids}}\
        {{#if ids.length>0}}\
            <div class="row"><div class="offset-by-two four columns">\
            <input type="button" class="button-primary" on-click="submit" value="Submit">\
            </div></div>\
        {{/if}}\
    </ul>';

    list = new Ractive({
        el: "#listEl",
        append: true,
        template: template1,
        data: {
            ids: []
        },
        removeItem: function ( index ) {
            this.splice( 'ids', index.i, 1 );
        }
    });
    list.on('add', function(e) {
        list.get("ids").push(e.node.value);
        e.node.value="";
        localSave();
    });
    list.on('delete', function (deets) {
        this.removeItem(deets.index)
        localSave();
    });

    if (localStorage.getItem(dateId) !== null) {
        JSON.parse(localStorage.getItem(dateId)).forEach(function(e) {
            list.get("ids").push(e);
        });
    }
    /**
     * Saves the chosen ids to localStorage under the current person's dateId
     * @pre None
     * @post Saves all of the ids in the ractive array "ids" to localStorage.`currentDateId`
     */
    function localSave() {
        localStorage[dateId] = JSON.stringify(list.get("ids"));
    }

    list.on('submit', function () {
        console.log(dateId);
        // Create and send post request.
        var formData = new FormData();
        formData.append("dateId", dateId);
        var picks = "";
        [].forEach.call(list.get("ids"), function(id){
            picks += id + ",";
        });
        formData.append("picks", picks);
        console.log("picks: " + picks);

        $.ajax("/savePicks", {
            type: "post",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function () {
                alert("Saved Your data");
            },
            error: function(data) {
                console.log("error saving list");
                console.log(data);
                //$("#spinner").hide();
                //$this.show();
            }
        });
    });
});