$(function(){
    Ractive.DEBUG=false;
    var button, template =
    "<a class='button' href='{{#if pressed}}{{elevated}}{{else}}{{normal}}{{/if}}'>\
        {{#if pressed}}Admin Login{{else}}Login{{/if}}\
    </a>";

    button = new Ractive({
        el: "#loginEl",
        template: template,
        data: {
            normal: "/login",
            elevated: "/admin/login",
            pressed: false,
            changed: false
        }
    });

    document.onkeydown = function(e){
        if (e.keyCode == 90) {
            if (!button.get("changed")) {
                button.toggle("pressed");
                button.toggle("changed");
            }
        }
    }

    document.onkeyup = function(e) {
        if (e.keyCode == 90) {
            if (button.get("changed")) {
                button.toggle("pressed");
                button.toggle("changed");
            }
        }
    }

});