# Speed Dating Software!

## About
This software is basically meant to be used as a live Tinder. Participants would show up to a location, log into this site with their personal device and use the auto-generated 'dateId' to log into the site along with their name. They submit the datId's of people they would like to get to know better.
At the end of the event, the admin clicks on a button that will match the participants and send emails to people that chose each other. Emails are also sent to everyone else to notify them they did not match with anyone.

## Installation
###### All code samples should work in Ubuntu 14.04

#### Install Java 1.7
```bash
sudo apt-get install openjdk-7-jdk
```

#### Install Play! Framework v. 2.1.0

```bash
sudo wget http://downloads.typesafe.com/play/2.1.0/play-2.1.0.zip
# using unzip library
# sudo apt-get -y install unzip
unzip play-2.1.0.zip -d ~
# remove play zip file
rm -f play-2.1.0.zip
# Add play folder to path using .bashrc
echo "export PATH=\$PATH:~/play-2.1.0" >> .bashrc
# Execute .bashrc script to have acces to `play` command right away
cd ~
. .bashrc
```

#### Install LAMP

```bash
sudo apt-get -y install apache2
mysqlinstalled=$(apt-cache policy mysql-server | grep -c "(none)")
if [ $mysqlinstalled -gt 0 ]; then
    echo "Installing mysql-server. Follow prompts to securly set up MySQL."
    sudo apt-get -y install mysql-server
    sudo mysql_secure_installation
fi
sudo apt-get -y install php5 php-pear php5-mysql
sudo apt-get -y install phpmyadmin
sudo service apache2 restart
```

Or you can just use tasksel

```bash
sudo apt-get install tasksel
sudo tasksel
# Then select lamp on the list using space bar and press <enter>
# Follow the prompts to configure
```

## Configuration

Create a database in mysql:
```sql
CREATE DATABASE speeddates;
```
#### Set application.conf values
Set db.default.user and db.default.password.
Set the HTTPS flag.
Set smtp.user, smtp.name, smtp.password.

## Run in production mode

```bash
# compile
play clean compile stage
# start
target/start -DapplyEvolutions.default=true -DapplyDownEvolutions.deafult=true -Dhttp.port=9001
#stop
play stop
```
